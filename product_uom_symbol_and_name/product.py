# -*- coding: utf-8 -*-
################################################################
#    License, author and contributors information in:          #
#    __openerp__.py file at the root folder of this module.    #
################################################################

from openerp.osv import fields, orm
from tools.translate import _

class product_uom(orm.Model):
    _inherit = 'product.uom'

    _order = "name"
    _columns = {
        'name': fields.char('Name', size=64, required=True, translate=True,
                            help='The symbol used for this unit of measure.'),
        'description': fields.char('Name', size=64, required=True,
                                   translate=True,
                                   help=('The full name of this unit of '
                                         'measure.')),
    }

product_uom()
