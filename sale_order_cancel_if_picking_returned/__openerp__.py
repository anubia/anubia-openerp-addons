# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) All rights reserved:
#        (c) 2014      Anubía, soluciones en la nube,SL (http://www.anubia.es)
#                      Alejandro Santana <alejandrosantana@anubia.es>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses
#
##############################################################################
{
    "name": "name",
    "version": "1.0",
    "depends": ["base"],
    "author": "Alejandro Santana <alejandrosantana@anubia.es>",
    "category": "Sales",
    "description": """
This module changes a sale order state to 'Shipping exception' whenever any
product is returned (any amount).
This way, customer has the capability to decide what to do with that sale 
order.

The use case for such functionality is the following:
    * A confirmed sales order is to be invoiced from stock picking.
    * The stock is delivered, but the whole thing is returned by the customer
      (maybe to an error in the sale order itself).
    * Without this module, the sale order would be in "Done" state, and 
      marked as "Delivered" and "Not paid". This looks strange in tree view
      and also makes it difficult to distinguish an order yet to be paid 
      from another that was returned fully.
    """,
    "init_xml": [],
    'update_xml': [],
    'demo_xml': [],
    'installable': True,
    'active': False,
#    'certificate': 'certificate',
}
