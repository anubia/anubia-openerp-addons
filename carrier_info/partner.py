# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2008 Zikzakmedia S.L. (http://zikzakmedia.com) All Rights Reserved.
#                       Jordi Esteve <jesteve@zikzakmedia.com>
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from osv import osv
from osv import fields

class res_partner(osv.osv):
    _inherit = 'res.partner'
    _columns = {
        'carrier': fields.boolean(
            'Carrier',
            help="Check if this partner is a carrier."),
        'carrier_vehicle': fields.text(
            'Vehicle data',
            help=("Any data related to the vehicles, like plates, "
                  "maximum weight, ship name, etc.")),
        'carrier_notes': fields.text(
            'Carrier notes',
            help=("Any other data regarding the carrier.")),
    }
    _defaults = {  
        'carrier': False,
    }

res_partner()
