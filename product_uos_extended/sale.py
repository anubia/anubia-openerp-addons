# -*- coding: utf-8 -*-
################################################################
#    License, author and contributors information in:          #
#    __openerp__.py file at the root folder of this module.    #
################################################################

import decimal_precision as dp
import logging
# from osv import fields, osv
from openerp.osv import fields, orm
from tools.translate import _


class sale_order(orm.Model):
    _inherit = "sale.order"

    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        return super(sale_order, self)._amount_all(cr, uid, ids, field_name,
                                                   arg, context=context)
#         cur_obj = self.pool.get('res.currency')
#         res = {}
#         for order in self.browse(cr, uid, ids, context=context):
#             res[order.id] = {
#                 'amount_untaxed': 0.0,
#                 'amount_tax': 0.0,
#                 'amount_total': 0.0,
#             }
#             val = val1 = 0.0
#             cur = order.pricelist_id.currency_id
#             for line in order.order_line:
#                 val1 += line.price_subtotal
#                 val += self._amount_line_tax(cr, uid, line, context=context)
#             res[order.id]['amount_tax'] = cur_obj.round(cr, uid, cur, val)
#             res[order.id]['amount_untaxed'] = cur_obj.round(cr, uid, cur, val1)
#             res[order.id]['amount_total'] = res[order.id]['amount_untaxed'] + res[order.id]['amount_tax']
#         return res

    def _get_order(self, cr, uid, ids, context=None):
#         keys = super(sale_order, self)._get_order(cr, uid, ids, context=context)
#         return keys
        result = {}
        for line in self.pool.get('sale.order.line').browse(cr, uid, ids, context=context):
            result[line.order_id.id] = True
        return result.keys()

    _columns = {
        'amount_untaxed': fields.function(_amount_all, digits_compute= dp.get_precision('Account'), string='Untaxed Amount',
            store = {
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The amount without tax."),
        'amount_tax': fields.function(_amount_all, digits_compute= dp.get_precision('Account'), string='Taxes',
            store = {
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The tax amount."),
        'amount_total': fields.function(_amount_all, digits_compute= dp.get_precision('Account'), string='Total',
            store = {
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The total amount."),
    }

sale_order()

class sale_order_line(orm.Model):
    """
Inherit sale_order_line to allow users to enter
price_unit_uos or price_unit (uom) and calculate the other values accordingly.
"""
    _inherit = "sale.order.line"

    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        return super(sale_order_line, self)._amount_line(cr, uid, ids,
                                                         field_name, arg,
                                                         context=context)
#         tax_obj = self.pool.get('account.tax')
#         cur_obj = self.pool.get('res.currency')
#         res = {}
#         if context is None:
#             context = {}
#         for line in self.browse(cr, uid, ids, context=context):
#             if line.product_uos:
#                 base_price = line.price_unit_uos
#                 qty = line.product_uos_qty
#             else:
#                 base_price = line.price_unit
#                 qty = line.product_uom_qty
#             price = base_price * (1 - (line.discount or 0.0) / 100.0)
#             taxes = tax_obj.compute_all(cr, uid, line.tax_id, price, qty,
#                                         line.order_id.partner_invoice_id.id,
#                                         line.product_id,
#                                         line.order_id.partner_id)
#             cur = line.order_id.pricelist_id.currency_id
#             res[line.id] = cur_obj.round(cr, uid, cur, taxes['total'])
#         return res

    def _get_uos_id(self, cr, uid, *args):
        try:
            proxy = self.pool.get('ir.model.data')
            result = proxy.get_object_reference(cr, uid, 'product',
                                                'product_uos_unit')
            return result[1]
        except Exception, ex:
            return False

    def _has_uos(self, cr, uid, *args):
        try:
            proxy = self.pool.get('ir.model.data')
            result = proxy.get_object_reference(cr, uid, 'product',
                                                'product_uos_unit')
            has_uos = result[1] and True or False
            return has_uos
        except Exception, ex:
            return False

    _columns = {
        'product_has_uos': fields.boolean(
            'Product has UoS defined',
            help=("Checked if selected product has a defined UoS "
                  "(Unit of Sale).")),
        'price_unit_uos': fields.float(
            'Unit Price (UoS)',
            required=True,
            digits_compute=dp.get_precision('Sale Price'),
            states={'draft': [('readonly', False)]}),
        'price_subtotal': fields.function(
            _amount_line, string='Subtotal',
            digits_compute = dp.get_precision('Account')),
        'product_uos_dummy': fields.related(
            'product_uos', type="many2one", relation="product.uom",
            readonly=True, store=False, string='Product UoS'), 
        'product_uos_qty_dummy': fields.related(
            'product_uos_qty', type="float",
            readonly=True, store=False, string='Quantity (UoS)',
            digits_compute=dp.get_precision('Product UoS')),
        'price_unit_uos_dummy': fields.related(
            'price_unit_uos', type="float",
            readonly=True, store=False, string='Unit Price (UoS)',
            digits_compute=dp.get_precision('Sale Price')),
        'cancel_uom': fields.boolean(
            'Cancel onchange_uom_id'),
        'cancel_uom_qty': fields.boolean(
            'Cancel onchange_quantity_uom'),
        'cancel_uom_price': fields.boolean(
            'Cancel onchange_price_unit_uom'),
        'cancel_uos': fields.boolean(
            'Cancel onchange_uos_id'),
        'cancel_uos_qty': fields.boolean(
            'Cancel onchange_quantity'),
        'cancel_uos_price': fields.boolean(
            'Cancel onchange_price_unit'),
        # NOTE: [0]
        # ---------
        # With some extra fields (starting with 'cancel_') we manage to
        # avoid mutual recursive triggers amongst fields that change
        # one another.
        # A) If this change is a result of direct manual user's intervention,
        #    such flag should be False, and normal execution should continue.
        # B) Otherwise, being a result other onchange functions started
        #    by this one, such flag should be True. In that case, further
        #    processing of this onchange is skipped, the flag reset to False
        #    (thus 
    }
    _defaults = {
        'cancel_uom': False,
        'cancel_uom_qty': False,
        'cancel_uom_price': False,
        'cancel_uos': True,
        'cancel_uos_qty': True,
        'cancel_uos_price': True,
        'price_unit_uos': 0.0,
        'product_uos' : _get_uos_id,
        'product_has_uos': _has_uos,
    }

    def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False,
                                         context=None):
        """Prepare the dict of values to create the new invoice line for a
           sale order line. This method may be overridden to implement custom
           invoice generation (making sure to call super() to establish
           a clean extension chain).

           :param browse_record line: sale.order.line record to invoice
           :param int account_id: optional ID of a G/L account to force
               (this is used for returning products including service)
           :return: dict of values to create() the invoice line
        """
        super(sale_order_line,
              self)._prepare_order_line_invoice_line(cr, uid, line, account_id,
                                                     context=context)

        def _get_line_qty(line):
            is_order = (line.order_id.invoice_quantity == 'order')
            if is_order or not line.procurement_id:
                if line.product_uos:
                    return line.product_uos_qty or 0.0
                return line.product_uom_qty
            else:
                return self.pool.get('procurement.order').quantity_get(
                    cr, uid, line.procurement_id.id, context=context)

        def _get_line_uom(line):
            is_order = (line.order_id.invoice_quantity == 'order')
            if is_order or not line.procurement_id:
                if line.product_uos:
                    return line.product_uos.id
                return line.product_uom.id
            else:
                return self.pool.get('procurement.order').uom_get(
                    cr, uid, line.procurement_id.id, context=context)

        def _get_line_price_unit_uos(line):
            is_order = (line.order_id.invoice_quantity == 'order')
            if is_order or not line.procurement_id:
                if line.product_uos:
                    return line.price_unit_uos
            return line.price_unit

        if not line.invoiced:
            if not account_id:
                if line.product_id:
                    account_id = line.product_id.product_tmpl_id.property_account_income.id
                    if not account_id:
                        account_id = line.product_id.categ_id.property_account_income_categ.id
                    if not account_id:
                        raise osv.except_osv(_('Error !'),
                                             _('There is no income account '
                                               'defined for this product:'
                                               ' "%s" (id:%d)') %
                                             (line.product_id.name,
                                              line.product_id.id,))
                else:
                    prop = self.pool.get('ir.property').get(
                        cr, uid, 'property_account_income_categ',
                        'product.category', context=context)
                    account_id = prop and prop.id or False
            uosqty = _get_line_qty(line)
            uos_id = _get_line_uom(line)
            pu = 0.0
            if uosqty:
                pu = _get_line_price_unit_uos(line)
                #pu = round(line.price_unit * line.product_uom_qty / uosqty,
                        #self.pool.get('decimal.precision').precision_get(cr,
                                # uid, 'Sale Price'))
            fpos = line.order_id.fiscal_position or False
            fpos_obj = self.pool.get('account.fiscal.position')
            account_id = fpos_obj.map_account(cr, uid, fpos, account_id)
            if not account_id:
                raise osv.except_osv(_('Error !'),
                                     _('There is no income category account '
                                       'defined in default Properties for '
                                       'Product Category or Fiscal Position '
                                       'is not defined !'))
            res = {
                'name': line.name,
                'origin': line.order_id.name,
                'account_id': account_id,
                'price_unit': pu,
                'quantity': uosqty,
                'discount': line.discount,
                'uos_id': uos_id,
                'product_has_uos': line.product_id.uos_id and True or False,
                'product_id': line.product_id.id or False,
                'invoice_line_tax_id': [(6, 0, [x.id for x in line.tax_id])],
                'note': line.notes,
                'account_analytic_id': (line.order_id.project_id and
                                        line.order_id.project_id.id or
                                        False),
                'quantity_uom': line.product_uom_qty,
                'uom_id': line.product_uom.id,
                'price_unit_uom': line.price_unit,
            }
            return res
        return False

    def product_id_change(self, cr, uid, ids, pricelist, product, qty=0,
            uom=False, qty_uos=0, uos=False, name='', partner_id=False,
            lang=False, update_tax=True, date_order=False, packaging=False,
            fiscal_position=False, flag=False, context=None):
        '''Detects if product has UoS, or if there is no product'''
        res = super(sale_order_line, self).product_id_change(
            cr, uid, ids, pricelist, product, qty, uom, qty_uos, uos, name,
            partner_id, lang, update_tax, date_order, packaging,
            fiscal_position, flag, context=context)
        res['value'].update({'product_uom_qty': 1.0})
#        Requires setting skip_qty_uos flag to True???!!
        if not product:
            res['value'].update({'product_has_uos': False})
            res['domain'].update({'product_uom': [('category_id', '!=', '')]})
            res['domain'].update({'product_uos': [('category_id', '!=', '')]})
            return res
        else:
            product_obj = self.pool.get('product.product')
            product = product_obj.browse(cr, uid, product, context=context)
            res['value'].update({'product_uom': product.uom_id.id})
            res['value'].update({'product_uos_dummy': product.uom_id.id})
            uom_category_id = product.uom_id.category_id.id
            # Restrain the UoM drop menu to the its category
            res['domain'].update({
                'product_uom': [('category_id', '=', uom_category_id)]})
            if product.uos_id:
                res['value'].update({'product_has_uos': True})
                uos_category_id = product.uos_id.category_id.id
                # Restrain the UoS drop menu to the its category
                res['domain'].update({
                    'product_uos': [('category_id', '=', uos_category_id)]})
            else:
                res['value'].update({'product_has_uos': False})
                # Set the UoS = UoM 
                res['value'].update({'product_uos': product.uom_id.id})
                res['value'].update({'product_uos_dummy': product.uom_id.id})
                # Set UoS domain = UoM domain
                res['domain'].update({
                    'product_uos': [('category_id', '=', uom_category_id)]})

        return res

    def onchange_product_uom_qty(self, cr, uid, ids,
                                 product_id=None,
                                 qty_uom=0.0, uom_id=None, price_unit_uom=1.0,
                                 qty_uos=0.0, uos_id=None, price_unit_uos=1.0,
                                 context=None):
        """Updates product_uos_qty, and th_weight."""
        logger = logging.getLogger(__name__)
        logger.debug('>>>> onchange_product_uom_qty -> context: {}'.format(context))
        value = {}
        if not context:
            context = {}
        if context.get('skip_uom_qty', False):
            context.update({'skip_uom_qty': False})
            return {'value': value, 'context': context}
        qty_uom = float(qty_uom)
        qty_uos = float(qty_uos)

        if not product_id:
            if (qty_uom != qty_uos):
                value.update({'product_uos_qty': qty_uom})
            return {'value': value, 'context': context}

        product_uom_obj = self.pool.get('product.uom')
        if uos_id:
            new_qty_uos = product_uom_obj._qty_swap_uom_uos(
                cr, uid, product_id, 'uom2uos', uom_id, qty_uom, uos_id)
        else:
            new_qty_uos = qty_uom

        # Calculate the quantity in product logistic units, for total weight
        product_obj = self.pool.get('product.product')
        product = product_obj.browse(cr, uid, product_id)
        def_uom_id = product.uom_id.id
        def_qty_uom = product_uom_obj._compute_qty(cr, uid,
                                                   uom_id, qty_uom, def_uom_id)
        th_weight = def_qty_uom * product.weight
        value.update({'th_weight': th_weight})

        diff1 = (new_qty_uos != qty_uos)
        if diff1:
            value.update({'product_uos_qty': new_qty_uos})
            value.update({'product_uos_qty_dummy': new_qty_uos})
        context.update({'skip_uos_qty': diff1})
        return {'value': value, 'context': context}

    def onchange_product_uom(self, cr, uid, ids,
                             product_id=None,
                             qty_uom=0.0, uom_id=None, price_unit_uom=1.0,
                             qty_uos=0.0, uos_id=None, price_unit_uos=1.0,
                             product_has_uos=False, context=None):
        """Updates product_uos_qty and price_unit_uos.
        Does not update th_weight."""
        logger = logging.getLogger(__name__)
        logger.debug('>>>> onchange_product_uom -> context: {}'.format(context))
        value = {}
        if not context:
            context = {}
        if context.get('skip_uom', False):
            context.update({'skip_uom': False})
            return {'value': value, 'context': context}
        qty_uom = float(qty_uom)
        qty_uos = float(qty_uos)
        
        if not product_id or not product_has_uos or not uos_id:
            value.update({'product_uos': uom_id})
            value.update({'product_uos_dummy': uom_id})
            new_qty_uos = qty_uom
            new_price_unit_uos = price_unit_uom
        else:
            product_uom_obj = self.pool.get('product.uom')
            new_qty_uos = product_uom_obj._qty_swap_uom_uos(
                cr, uid, product_id, 'uom2uos', uom_id, qty_uom, uos_id)
            new_price_unit_uos = product_uom_obj._price_swap_uom_uos(
                cr, uid, product_id, 'uom2uos', uom_id, price_unit_uom, uos_id)

        diff1 = (new_qty_uos != qty_uos)
        if diff1:
            value.update({'product_uos_qty': new_qty_uos})
        context.update({'skip_uos_qty': diff1})

        diff2 = (new_price_unit_uos != price_unit_uos)
        if diff2:
            value.update({'price_unit_uos': new_price_unit_uos})
        context.update({'skip_uos_price': diff2})
        return {'value': value, 'context': context}

    def onchange_price_unit_uom(self, cr, uid, ids,
                                product_id=None,
                                qty_uom=0.0, uom_id=None, price_unit_uom=1.0,
                                qty_uos=0.0, uos_id=None, price_unit_uos=1.0,
                                context=None):
        """Updates product price_unit_uos"""
        logger = logging.getLogger(__name__)
        logger.debug('>>>> onchange_price_unit_uom -> context: {}'.format(context))
        value = {}
        if not context:
            context = {}
        if context.get('skip_uom_price', False):
            context.update({'skip_uom_price': False})
            return {'value': value, 'context': context}
        qty_uom = float(qty_uom)
        qty_uos = float(qty_uos)

        product_uom_obj = self.pool.get('product.uom')
        if uos_id:
            new_price_unit_uos = product_uom_obj._price_swap_uom_uos(
                cr, uid, product_id, 'uom2uos', uom_id, price_unit_uom, uos_id)
        else:
            new_price_unit_uos = price_unit_uom

        diff1 = (new_price_unit_uos != price_unit_uos)
        if diff1:
            value.update({'price_unit_uos': new_price_unit_uos})
            value.update({'price_unit_uos_dummy': new_price_unit_uos})
        context.update({'skip_uos_price': diff1})
        return {'value': value, 'context': context}

    #def uos_change(self, cr, uid, ids,
                   #product_uos, product_uos_qty=0, product_id=None):
    def onchange_product_uos_qty(self, cr, uid, ids,
                                 product_id=None,
                                 qty_uom=0.0, uom_id=None, price_unit_uom=1.0,
                                 qty_uos=0.0, uos_id=None, price_unit_uos=1.0,
                                 context=None):
        """ Updates product_qty_uom or product_qty_uos, and weight """
        logger = logging.getLogger(__name__)
        logger.debug('>>>> onchange_product_uos_qty -> context: {}'.format(context))
        value = {}
        value.update({'product_uos_qty_dummy': qty_uos})
        if not context:
            context = {}
        if context.get('skip_uos_qty', False):
            context.update({'skip_uos_qty': False})
            return {'value': value, 'context': context}
        qty_uom = float(qty_uom)
        qty_uos = float(qty_uos)

        product_uom_obj = self.pool.get('product.uom')
        if uos_id:
            new_qty_uom = product_uom_obj._qty_swap_uom_uos(
                cr, uid, product_id, 'uos2uom', uos_id, qty_uos, uom_id)
        else:
            new_qty_uom = qty_uos

        diff1 = (new_qty_uom != qty_uom)
        if diff1:
            value.update({'product_uom_qty': new_qty_uom})
        context.update({'skip_uom_qty': diff1})
        return {'value': value, 'context': context}

    def onchange_product_uos(self, cr, uid, ids,
                             product_id=None,
                             qty_uom=0.0, uom_id=None, price_unit_uom=1.0,
                             qty_uos=0.0, uos_id=None, price_unit_uos=1.0,
                             product_has_uos=False, context=None):
        """Updates product_uom_qty (if uos_id is defined) or
        updates product_uos_qty (if uos_id is not defined)."""
        logger = logging.getLogger(__name__)
        logger.debug('>>>> onchange_product_uos -> context: {}'.format(context))
        value = {}
        if not context:
            context = {}
        if context.get('skip_uos', False):
            context.update({'skip_uos': False})
            return {'value': value, 'context': context}
        qty_uom = float(qty_uom)
        qty_uos = float(qty_uos)

        if not product_id or not uos_id or not product_has_uos:
            # If uos not defined, we take uom values for qty and price
            value.update({'product_uos_qty': qty_uom})
            value.update({'product_uos_qty_dummy': qty_uom})
            value.update({'price_unit_uos': price_unit_uom})
            value.update({'price_unit_uos_dummy': price_unit_uom})
            context.update({'skip_uom_qty': True})
            context.update({'skip_uom_price': True})
            return {'value': value, 'context': context}

#         context.update({'skip_uom_qty': False})
#         context.update({'skip_uom_price': False})

        product_uom_obj = self.pool.get('product.uom')
        new_qty_uom = product_uom_obj._qty_swap_uom_uos(
            cr, uid, product_id, 'uos2uom', uos_id, qty_uos, uom_id)
        new_price_unit_uom = product_uom_obj._price_swap_uom_uos(
            cr, uid, product_id, 'uos2uom', uos_id, price_unit_uos, uom_id)

        diff1 = (new_qty_uom != qty_uom)
        if diff1:
            value.update({'product_uom_qty': new_qty_uom})
        context.update({'skip_uom_qty': diff1})

        diff2 = (new_price_unit_uom != price_unit_uom)
        if diff2:
            value.update({'price_unit': new_price_unit_uom})
        context.update({'skip_uom_price': diff2})
        return {'value': value, 'context': context}

    def onchange_price_unit_uos(self, cr, uid, ids,
                                product_id=None,
                                qty_uom=0.0, uom_id=None, price_unit_uom=1.0,
                                qty_uos=0.0, uos_id=None, price_unit_uos=1.0,
                                context=None):
        """Updates product price_unit"""
        logger = logging.getLogger(__name__)
        logger.debug('>>>> onchange_price_unit_uos -> context: {}'.format(context))
        value = {}
        value.update({'price_unit_uos_dummy': price_unit_uos})
        if not context:
            context = {}
        if context.get('skip_uos_price', False):
            context.update({'skip_uos_price': False})
            return {'value': value, 'context': context}
        qty_uom = float(qty_uom)
        qty_uos = float(qty_uos)

        product_uom_obj = self.pool.get('product.uom')
        new_price_unit_uom = product_uom_obj._price_swap_uom_uos(
            cr, uid, product_id, 'uos2uom', uos_id, price_unit_uos, uom_id)

        diff1 = (new_price_unit_uom != price_unit_uom)
        if diff1:
            value.update({'price_unit': new_price_unit_uom})
        context.update({'skip_uos_price': diff1})
        return {'value': value, 'context': context}

sale_order_line()
