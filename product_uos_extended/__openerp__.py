# -*- encoding: utf-8 -*-
##############################################################################
#
#    Author: Alejandro Santana <alejandrosantana@anubia.es>
#    Copyright (c) All rights reserved:
#        (c) 2013      Anubía, soluciones en la nube,SL (http://www.anubia.es)
#                      Alejandro Santana <alejandrosantana@anubia.es>
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    # product_uos_extended
    "name": "Unit of Sale price in product, sales order and invoices",
    "version": "1.1",
    "depends": [
        "product",
        "sale",
        "account",
    ],
    "author": "Alejandro Santana <alejandrosantana@anubia.es>",
    "category": "Product/Sale/Accounting",
    "description": """
This module aims to :
    * If a 'Unit of Sale' (UoS) is specified for a product,
      add the possibility to also specify the list price per UoS instead of
      list price per 'Unit of Measure' (UoM).
    * When updating list price (per UoS or per UoM), UoS or UoS coefficient
      the others will be updated accordingly.
    * This applies for products, sales orders and invoices (still WIP).
""",
    "init_xml": [],
    'update_xml': [
        "product_view.xml",
        "sale_view.xml",
        "stock_view.xml",
        "account_invoice_view.xml",
        "account_report.xml",
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
    #'certificate': 'certificate',
}
